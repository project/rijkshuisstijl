<?php


/**
 * @file
 * Default theme implementation to display a single Drupal page while offline.
 *
 * All the available variables are mirrored in html.tpl.php and page.tpl.php.
 * Some may be blank but they are provided for consistency.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 *
 * @ingroup themeable
 */

?>
<!DOCTYPE html>
<html class="no-js">
<head>
  <?php print $head; ?>
  <title>
    <?php print $head_title; ?>
  </title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <!--[if lt IE 9]>
    <script src="<?php print base_path() . path_to_theme(); ?>/javascripts/html5shiv.js"></script>
    <script src="<?php print base_path() . path_to_theme(); ?>/javascripts/respond.min.js"></script>
  <![endif]-->
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="centerbox" role="document">
    <div id="mainbox">
      <header id="header" role="banner" class="clearfix">
        <?php if ($projectlogo && $branding_type == 'neutral'): ?>
          <div id="projectlogo" >
          <?php print $projectlogo; ?>
          </div>
        <?php endif; ?>

        <?php if ($rijkslogo && $branding_type == 'rijkshuisstijl'): ?>
          <div id="rijkslogo" >
          <?php print $rijkslogo; ?>
          </div>
        <?php endif; ?>

        <?php if ($site_name): ?>
          <div id="title-bar">
              <?php print $site_name; ?>
          </div>
        <?php endif; ?>
      </header>

      <div id="main" role="main" class="clearfix">
        <div class="header">
          <?php if ($title): ?>
            <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
            <?php endif; ?>
        </div>
        <div id="content" role="article" class="column article">
          <?php if ($messages): ?>
            <div id="messages" role="alertdialog">
              <?php print $messages; ?>
            </div>
          <?php endif; ?>
          <?php print $content; ?>
        </div>
      </div>
      <?php if ($branding_type == "rijkshuisstijl"): ?>
        <p id="payoff">
          <?php if ($pay_off_footer): ?>
            <span><?php print $pay_off_footer; ?></span>
          <?php endif; ?>
        </p>
      <?php endif; ?>
    </div>

    <footer id="footer" role="contentinfo">
    </footer>
  </div>

</body>
</html>
