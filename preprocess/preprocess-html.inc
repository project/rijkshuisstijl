<?php
/**
 * @file
 * Implements hook_preprocess_html().
 */

global $theme_key, $language;
$theme_name = $theme_key;

// DOCTYPE.
$vars['doctype'] = '<!DOCTYPE html>' . "\n";
$vars['rdf_profile'] = '';

// Use a proper attributes array for the html attributes.
$vars['html_attributes_array']['lang'][] = $language->language;
$vars['html_attributes_array']['dir'][] = $language->dir;

// Add mobile viewport.
$viewport = array(
  '#tag' => 'meta',
  '#attributes' => array(
    'name' => 'viewport',
    'content' => 'width=device-width,initial-scale=1.0',
  ),
);
drupal_add_html_head($viewport, 'viewport');

// Add semantic markup for Google.
if (drupal_is_front_page()) {
  // Add the Site Name to the Google Results.
  // See: https://developers.google.com/structured-data/site-name
  $json_data = array(
    '@context' => 'http://schema.org',
    '@type' => 'WebSite',
    'name' => variable_get('site_name'),
    'url' => url('<front>', array('absolute' => TRUE)),
  );

  // If Drupal core search is being used, add the Sitelinks Search Box too.
  // See: https://developers.google.com/structured-data/slsb-overview
  if (module_exists('search')) {
    $json_data['potentialAction'] = array(
      '@type' => 'SearchAction',
      'target' => url('search/node/', array('absolute' => TRUE)) . '{search_block_form}',
      'query-input' => 'name=search_block_form',
    );
  }

  $script_tag = array(
    '#tag' => 'script',
    '#attributes' => array(
      'type' => 'application/ld+json',
    ),
    '#value' => drupal_json_encode($json_data),
  );
  drupal_add_html_head($script_tag, 'google_site_results');
}
