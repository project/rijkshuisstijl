<?php
/**
 * @file
 * Implements hook_preprocess_page().
 */

global $theme_key;
$theme_name = $theme_key;

// Hide title on frontpage.
if ($vars['is_front']) {
  $vars['title_attributes_array']['class'][] = 'element-invisible';
}

// Display an error if the website is not allowed to use the Rijkshuisstijl.
if (theme_get_setting('authorisation') == FALSE && theme_get_setting('branding_type') != 'none') {
  $params = array(
    '@theme-link' => url('admin/appearance/settings/rijkshuisstijl', array(
      'fragment' => 'edit-authorisation',
    )),
    '@gov-link' => url('https://www.rijkshuisstijl.nl/over-de-rijkshuisstijl/huisstijlcoordinatoren'),
  );
  $message = t('This website is not yet authorised to use the visual identity of the State of the Netherlands. To obtain authorisation, you should apply to the <a href="@gov-link">Government Information Service</a> and <a href="@theme-link">update the theme settings</a>.', $params);
  drupal_set_message($message, 'error');
}

/*
 * Set up logo's and wordmark for this website.
 */

// Provide a variable with the type of branding (rijkshuisstijl|neutral|custom).
$vars['branding_type'] = theme_get_setting('branding_type', $theme_name);

// Provide variables with the header and footer payoff.
$vars['pay_off_header'] = theme_get_setting('pay_off_header');
$vars['pay_off_footer'] = theme_get_setting('pay_off_footer');

/*
 * Add the sender of the website to place on the right of the logotype of the
 * Rijksoverheid logo. i.e. National Archives, secondary sender: Ministry of
 * Education. May contain linebreaks for long names, should get converted to
 * <br /> not sure if we for a11y should include the wordmark in a p, or
 * add headings etc.
 */
$sender = theme_get_setting('sender');
$vars['sender'] = '<span class="sender">' . nl2br(check_plain($sender)) . '</span>';
$secondary_sender = theme_get_setting('secondary_sender');
$vars['secondary_sender'] = '<span class="secondary-sender">' . nl2br(check_plain($secondary_sender)) . '</span>';
$vars['wordmark'] = ($sender) ? '<span class="wordmark">' . $vars['sender'] . ' ' . $vars['secondary_sender'] . '</span>' : "";

$logo_default_vars = array(
  'width' => 44,
  'height' => 77,
  'alt' => t('Logo of @sender - @secondary_sender', array(
    '@sender' => $sender,
    '@secondary_sender' => $secondary_sender),
  ),
  'attributes' => array('class' => 'logotype'),
);
$vars['rijkslogo_img_svg'] = theme('image', array('path' => path_to_theme() . '/images/basislogo.svg') + $logo_default_vars);
$vars['rijkslogo_img_png'] = theme('image', array('path' => path_to_theme() . '/images/basislogo.png') + $logo_default_vars);

// Include smart SVG fallback with PNG for browsers that don't support SVG
// (IE8 and Android 2).
$vars['rijkslogo_img_with_fallback'] = '<!--[if lte IE 8]>' . $vars['rijkslogo_img_png'] . '<![endif]-->' .
  '<!--[if gt IE 8]>' . $vars['rijkslogo_img_svg'] . '<![endif]-->' .
  '<!--[if !IE]> -->' . $vars['rijkslogo_img_svg'] . '<!-- <![endif]-->';

/*
 * Fallback mechanism described here: http://stackoverflow.com/questions/8946134/conditional-code-for-displaying-png-in-place-of-svg-for-older-browsers
 * Note there are 10+ fallback tricks for svg, none of them is perfect.
 * Above one should work for every browser, even without javascript,
 * and works for IE8 and Android 2. Not sure if it works for IE10
 * Drawback is performance as an extra file request is done
 * (PNG is loaded also by modern browsers).
 */

// Combining above to a styled and linked Rijkslogo.
$vars['rijkslogo'] = l($vars['rijkslogo_img_with_fallback'] . $vars['wordmark'], '<front>', array(
  'attributes' => array('title' => t('Home page')),
  'html' => TRUE,
));

// Generate projectlogo (wordmark, rendered as text with markup) used in
// the neutrale stijl.
$projecttext = theme_get_setting('project_name');
$vars['projectlogo_html'] = strtr(nl2br(check_plain($projecttext)), array('{' => '<span>', '}' => '</span>'));
$vars['projectlogo'] = $projecttext ? l($vars['projectlogo_html'], '<front>', array('attributes' => array('title' => t('Home page')), 'html' => TRUE)) : "";

/*
 * @todo: Generate a custom logo from a file upload, for special edge cases.
 * if ($branding_type == 'custom'){
 *    $logo_path = check_url($vars['logo']);
 * }
 * $vars['site_logo'] = drupal_static('rijkshuisstijl_preprocess_page_site_logo');
 */
