Rijkshuisstijl
--------------


This theme uses Bundler (http://bundler.io/)
  $ gem install bundler


In your console go to your theme root (../sites/[all]/themes/rijkshuisstijl)
  $ bundle install


Tell Compass to 'watch' our project and detect changes
  $ bundle exec compass watch

It should say: >>> Compass is polling for changes. Press Ctrl-C to Stop.
