##Create a Rijkshuisstijl sub theme.

1. Copy and paste rijkshuisstijl_subtheme. It doesn't matter where you place
   the copied version as long as its in a theme directory. For example if you
   are using sites/all/themes you can place it there - so you end up with:

   sites/all/themes/theme_name

2. Rename the info file and edit the info file details. For example let's assume
   you want your theme to be called "sub_theme", the name of the info file will
   be "sub_theme.info". Once you have renamed the file open it up and change
   the "name" to sub_theme and change the description to suit your taste.

3. When you have copied the sub theme in sites/all/themes you can skip this
   step. Edit the config.rb file in the root of your sub theme and change the
   following rule: ``add_import_path "../rijkshuisstijl/sass"`` to the correct
   path to the sass folder of the base theme.

When you now compile your sub theme's sass with ``bundle exec compass watch``
the sass files of the base theme will also be compiled in your sub theme's
css file.

If you want to change the color scheme to, for example green, all you have to
do is to change the 3 Main color variables.

<pre><code>$mainColor : $azure;
$mainColor-lighter : $azure-lighter;
$mainColor-lightest : $azure-lightest;
</code></pre>
